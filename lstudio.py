import os
import sys

print("""
					+----------------------------+
					|        lstudio v1.0        |
					+----------------------------+

	\033[1;32m [+] Author: Labreche Abdelatif \033[1;m
 	\033[1;32m [+] lstudio: Helps you to install all linux supported multimedia programs \033[1;m

 +---------------------+---------------+-----------------+-------------+--------------+
 |        Audio        |      Video    |       Image     |      3D     |   Animation  |
 +---------------------+---------------+-----------------+-------------+--------------+
 | 1) vlc Media Player | 3) Lightworks | 5) Gimp         | 8) Blender  | 10) Natron   |
 | 2) Audacity         | 4) Openshot   | 6) Raw Therapee | 9) Openscad |              | 
 |                     |               | 7) Inkscape     |             |              | 
 +---------------------+---------------+-----------------+-------------+--------------+
 0) Exit
""")
while True:
	cmd = int(input("\033[1;34mlstudio > \033[1;m"))

	if(cmd == 1):
		os.system("sudo pacman -S vlc")
	elif(cmd == 2):
		os.system("sudo pacman -S audacity")
	elif(cmd == 3):
		os.system("yaourt -S lwks")
	elif(cmd == 4):
		os.system("sudo pacman -S openshot")
	elif(cmd == 5):
		os.system("sudo pacman -S gimp")
	elif(cmd == 6):
		os.system("sudo pacman -S rawtherapee")
	elif(cmd == 7):
  		os.system("sudo pacman -S inkscape")
	elif(cmd == 8):
		os.system("sudo pacman -S blender")
	elif(cmd == 9):
		os.system("sudo pacman -S openscad")
	elif(cmd == 10):
		os.system("yaourt -S gdebi")
	elif(cmd == 0):
		sys.exit()
